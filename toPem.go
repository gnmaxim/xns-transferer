package main

import (
	"encoding/pem"
	"fmt"
	"math/big"
	"os"

	xecdsa "github.com/insolar/x-crypto/ecdsa"
	xelliptic "github.com/insolar/x-crypto/elliptic"
	"github.com/insolar/x-crypto/x509"
)

func main() {
	// this is private key string from web ui
	hexPriv := "e062dd86e7a6d821b1269ca04bbf2307a96c96bdd0b8be60db3d998a11467927"
	i := new(big.Int)
	i.SetString(hexPriv, 16)

	priv := new(xecdsa.PrivateKey)
	priv.PublicKey.Curve = xelliptic.P256K()
	priv.D = i
	priv.PublicKey.X, priv.PublicKey.Y = xelliptic.P256K().ScalarBaseMult(i.Bytes())

	x509Encoded, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		panic(err)
	}
	privKeyPem := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})

	x509EncodedPub, err := x509.MarshalPKIXPublicKey(&priv.PublicKey)
	if err != nil {
		panic(err)
	}
	pubKeyPem := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	fmt.Println(string(privKeyPem))
	fmt.Println(string(pubKeyPem))

	file, err := os.Create("private.pem")
	if err != nil {
		fmt.Println(err)
		return
	}
	file.WriteString(string(privKeyPem))
	file.Close()
}
