package main

import (
	// You will need:
	// - Some basic Golang functionality.
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"

	// - HTTP client and a cookiejar.
	"net/http"
	"net/http/cookiejar"

	"golang.org/x/net/publicsuffix"

	// - Big numbers to store signatures.
	"math/big"
	// - Basic cryptography.
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"

	// - Basic encoding capabilities.
	"encoding/asn1"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
)

// Declare a structure to contain the ECDSA signature:
type ecdsaSignature struct {
	R, S *big.Int
}

// Set the endpoint URL for the testing environment:
const (
	MainNetURL = "https://wallet-api.test.insolar.io/api/rpc"
)

// Create and initialize an HTTP client for connection re-use and put a cookiejar into it:
var client *http.Client
var jar cookiejar.Jar

func init() {
	// All users of cookiejar should import "golang.org/x/net/publicsuffix"
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		log.Fatal(err)
	}
	client = &http.Client{
		Jar: jar,
	}
}

// Create a variable for the JSON RPC 2.0 request identifier:
// The identifier is to be incremented for every request and each corresponding response will contain it.
var id int = 1

type requestBody struct {
	// Declare a nested structure to form requests to Insolar API in accordance with the specification.
	// The Platform uses the basic JSON RPC 2.0 request structure:
	JSONRPC string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Method  string `json:"method"`
}

type requestBodyWithParams struct {
	JSONRPC string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Method  string `json:"method"`
	// Params is a structure that depends on a particular method:
	Params interface{} `json:"params"`
}

type params struct {
	// The Platform defines params of the signed request as follows:
	Seed     string `json:"seed"`
	CallSite string `json:"callSite"`
	// CallParams is a structure that depends on a particular method.
	CallParams interface{} `json:"callParams"`
	PublicKey  string      `json:"publicKey"`
}

type paramsWithReference struct {
	params
	Reference string `json:"reference"`
}

// The member.create request has no parameters, so it's an empty structure:
type memberCreateCallParams struct{}

// The transfer request sends an amount of funds to member identified by a reference:
type transferCallParams struct {
	Amount            string `json:"amount"`
	ToMemberReference string `json:"toMemberReference"`
}

// Create a function to get a new seed for each signed request:
func getNewSeed() string {
	// Form a request body for getSeed:
	getSeedReq := requestBody{
		JSONRPC: "2.0",
		Method:  "node.getSeed",
		ID:      id,
	}
	// Increment the id for future requests:
	id++

	// Marshal the payload into JSON:
	jsonSeedReq, err := json.Marshal(getSeedReq)
	if err != nil {
		log.Fatalln(err)
	}

	// Create a new HTTP request and send it:
	seedReq, err := http.NewRequest("POST", MainNetURL, bytes.NewBuffer(jsonSeedReq))
	if err != nil {
		log.Fatalln(err)
	}
	seedReq.Header.Set("Content-Type", "application/json")

	// Perform the request:
	seedResponse, err := client.Do(seedReq)
	if err != nil {
		log.Fatalln(err)
	}
	defer seedReq.Body.Close()

	// Receive the response body:
	seedRespBody, err := ioutil.ReadAll(seedResponse.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Unmarshal the response:
	var newSeed map[string]interface{}
	err = json.Unmarshal(seedRespBody, &newSeed)
	if err != nil {
		log.Fatalln(err)
	}

	// (Optional) Print the request and its response:
	print := "POST to " + MainNetURL +
		"\nPayload: " + string(jsonSeedReq) +
		"\nResponse status code: " + strconv.Itoa(seedResponse.StatusCode) +
		"\nResponse: " + string(seedRespBody) + "\n"
	fmt.Println(print)

	// Retrieve and return the current seed:
	return newSeed["result"].(map[string]interface{})["seed"].(string)
}

// Create a function to send signed requests:
func sendSignedRequest(payload requestBodyWithParams, privateKey *ecdsa.PrivateKey) map[string]interface{} {
	// Marshal the payload into JSON:
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		log.Fatalln(err)
	}

	// Take a SHA-256 hash of the payload's bytes:
	hash := sha256.Sum256(jsonPayload)

	// Sign the hash with the private key:
	r, s, err := ecdsa.Sign(rand.Reader, privateKey, hash[:])
	if err != nil {
		log.Fatalln(err)
	}

	// Convert the signature into ASN.1 DER format:
	sig := ecdsaSignature{
		R: r,
		S: s,
	}
	signature, err := asn1.Marshal(sig)
	if err != nil {
		log.Fatalln(err)
	}

	// Convert both hash and signature into a Base64 string:
	hash64 := base64.StdEncoding.EncodeToString(hash[:])
	signature64 := base64.StdEncoding.EncodeToString(signature)

	// Create a new request and set its headers:
	request, err := http.NewRequest("POST", MainNetURL, bytes.NewBuffer(jsonPayload))
	if err != nil {
		log.Fatalln(err)
	}
	request.Header.Set("Content-Type", "application/json")

	// Put the hash string into the HTTP Digest header:
	request.Header.Set("Digest", "SHA-256="+hash64)

	// Put the signature string into the HTTP Signature header:
	request.Header.Set("Signature", "keyId=\"public-key\", algorithm=\"ecdsa\", headers=\"digest\", signature="+signature64)

	// Send the signed request:
	response, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
	}
	defer response.Body.Close()

	// Receive the response body:
	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Unmarshal it into a JSON object:
	var JSONObject map[string]interface{}
	err = json.Unmarshal(responseBody, &JSONObject)
	if err != nil {
		log.Fatalln(err)
	}

	// (Optional) Print the request and its response:
	print := "POST to " + MainNetURL +
		"\nPayload: " + string(jsonPayload) +
		"\nResponse status code: " + strconv.Itoa(response.StatusCode) +
		"\nResponse: " + string(responseBody) + "\n"
	fmt.Println(print)

	// Return the response:
	return JSONObject
}

// Create the main function to form and send signed requests:
func main() {
	// Generate a key pair:

	privateKey := new(ecdsa.PrivateKey)
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	fmt.Println(privateKey)

	var publicKey ecdsa.PublicKey
	publicKey = privateKey.PublicKey
	fmt.Println("pub", publicKey)

	// Convert both private and public keys into PEM format:
	x509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		log.Fatalln(err)
	}
	pemPublicKey := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509PublicKey})

	x509PrivateKey, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		log.Fatalln(err)
	}
	pemPrivateKey := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509PrivateKey})

	// The private key is required to sign requests.
	// Make sure to put into a file to save it in some secure place later:
	file, err := os.Create("private.pem")
	if err != nil {
		fmt.Println(err)
		return
	}
	file.WriteString(string(pemPrivateKey))
	file.Close()

	// Get a seed to form the request:
	seed := getNewSeed()

	// Form a request body for member.create:
	createMemberReq := requestBodyWithParams{
		JSONRPC: "2.0",
		Method:  "contract.call",
		ID:      id,
		Params: params{
			Seed:       seed,
			CallSite:   "member.create",
			CallParams: memberCreateCallParams{},
			PublicKey:  string(pemPublicKey)},
	}
	// Increment the JSON RPC 2.0 request identifier for future requests:
	id++

	// Send the signed member.create request:
	newMember := sendSignedRequest(createMemberReq, privateKey)

	// Put the reference to your new member into a variable to send transfer requests:
	memberReference := newMember["result"].(map[string]interface{})["callResult"].(map[string]interface{})["reference"].(string)
	fmt.Println("Member reference is " + memberReference)

	// Get a new seed to form a transfer request:
	seed = getNewSeed()
	// Form a request body for transfer:
	transferReq := requestBodyWithParams{
		JSONRPC: "2.0",
		Method:  "contract.call",
		ID:      id,
		Params: paramsWithReference{params: params{
			Seed:     seed,
			CallSite: "member.transfer",
			CallParams: transferCallParams{
				Amount:            "1",
				ToMemberReference: "insolar:1Aqcg7Mo1BLw6gMFoDnzpNIYAF7DRJV3eKF9kazGOI5o",
			},
			PublicKey: string(pemPublicKey),
		},
			Reference: string(memberReference),
		},
	}
	// Increment the id for future requests:
	id++

	// Send the signed transfer request:
	newTransfer := sendSignedRequest(transferReq, privateKey)
	fee := newTransfer["result"].(map[string]interface{})["callResult"].(map[string]interface{})["fee"].(string)

	// (Optional) Print out the fee.
	fmt.Println("Fee is " + fee)
}
